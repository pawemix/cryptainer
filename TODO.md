
# ToDos

* [ ] Write README.
* [ ] Start some unit tests.
* [ ] Lazily ask for the password.
* [ ] Handle (GPG) key files.
* [ ] Handle multiple passwords.
* [ ] Blur the password input.

